<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $addressType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $addressType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $addressType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Address Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Addresses'), ['controller' => 'Addresses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Address'), ['controller' => 'Addresses', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="addressTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($addressType) ?>
    <fieldset>
        <legend><?= __('Edit Address Type') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
