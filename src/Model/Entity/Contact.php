<?php
namespace ContactManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $other
 * @property bool|null $active
 * @property int $contact_type_id
 * @property int $foreign_key
 * @property string $model
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \ContactManager\Model\Entity\ContactType $contact_type
 * @property \ContactManager\Model\Entity\ForeignKey $foreign_key
 */
class Contact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'phone' => true,
        'other' => true,
        'active' => true,
        'contact_type_id' => true,
        'foreign_key' => true,
        'model' => true,
        'created' => true,
        'modified' => true,
        'contact_type' => true,
        'foreign_key' => true
    ];
}
