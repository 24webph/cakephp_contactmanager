<?php
namespace ContactManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity
 *
 * @property int $id
 * @property string|null $label
 * @property string|null $address
 * @property string|null $city
 * @property string|null $postal_code
 * @property string|null $state
 * @property int|null $country_id
 * @property bool|null $active
 * @property int|null $address_type_id
 * @property int $foreign_key
 * @property string $model
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \ContactManager\Model\Entity\Country $country
 * @property \ContactManager\Model\Entity\AddressType $address_type
 */
class Address extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'label' => true,
        'address' => true,
        'city' => true,
        'postal_code' => true,
        'state' => true,
        'country_id' => true,
        'active' => true,
        'address_type_id' => true,
        'foreign_key' => true,
        'model' => true,
        'created' => true,
        'modified' => true,
        'country' => true,
        'address_type' => true
    ];
}
