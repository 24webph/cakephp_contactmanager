<?php
namespace ContactManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AddressTypes Model
 *
 * @property \ContactManager\Model\Table\AddressesTable|\Cake\ORM\Association\HasMany $Addresses
 *
 * @method \ContactManager\Model\Entity\AddressType get($primaryKey, $options = [])
 * @method \ContactManager\Model\Entity\AddressType newEntity($data = null, array $options = [])
 * @method \ContactManager\Model\Entity\AddressType[] newEntities(array $data, array $options = [])
 * @method \ContactManager\Model\Entity\AddressType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ContactManager\Model\Entity\AddressType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ContactManager\Model\Entity\AddressType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \ContactManager\Model\Entity\AddressType[] patchEntities($entities, array $data, array $options = [])
 * @method \ContactManager\Model\Entity\AddressType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AddressTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('address_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Addresses', [
            'foreignKey' => 'address_type_id',
            'className' => 'ContactManager.Addresses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }
}
