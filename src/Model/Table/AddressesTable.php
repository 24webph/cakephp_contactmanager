<?php
namespace ContactManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Addresses Model
 *
 * @property \ContactManager\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \ContactManager\Model\Table\AddressTypesTable|\Cake\ORM\Association\BelongsTo $AddressTypes
 *
 * @method \ContactManager\Model\Entity\Address get($primaryKey, $options = [])
 * @method \ContactManager\Model\Entity\Address newEntity($data = null, array $options = [])
 * @method \ContactManager\Model\Entity\Address[] newEntities(array $data, array $options = [])
 * @method \ContactManager\Model\Entity\Address|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ContactManager\Model\Entity\Address saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ContactManager\Model\Entity\Address patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \ContactManager\Model\Entity\Address[] patchEntities($entities, array $data, array $options = [])
 * @method \ContactManager\Model\Entity\Address findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AddressesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('addresses');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LocationManager.Location');

        $this->belongsTo('AddressTypes', [
            'foreignKey' => 'address_type_id',
            'joinType' => 'INNER',
            'className' => 'ContactManager.AddressTypes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('label')
            ->maxLength('label', 100)
            ->allowEmptyString('label');

        $validator
            ->scalar('address')
            ->maxLength('address', 400)
            ->allowEmptyString('address');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->allowEmptyString('city');

        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 100)
            ->allowEmptyString('postal_code');

        $validator
            ->scalar('state')
            ->maxLength('state', 100)
            ->allowEmptyString('state');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->scalar('model')
            ->maxLength('model', 100)
            ->requirePresence('model', 'create')
            ->allowEmptyString('model', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['address_type_id'], 'AddressTypes'));
        return $rules;
    }
}
