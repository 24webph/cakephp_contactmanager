<?php

/**
 * AddressBehavior
 *
 * PHP version 7
 * 
 * @category Behaviour
 * @package  24WEB.Address
 * @version  V1
 * @author   Paulo Homem <paulo.homem@24web.pt>
 * @license  
 * @link     http://24web.pt
 */

namespace ContactManager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\RulesChecker;

class AddressBehavior extends Behavior {

   public function initialize(array $config) {

        $this->setTableSettings();
    }

    // In a table or behavior class
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        
    }
   
    private function setTableSettings() {
        $this->getTable()->hasMany('ContactManager.Addresses')
                ->setForeignKey('foreign_key')
                ->setConditions(['model' => $this->getTable()->getAlias()]);
        return;
       
    }
    
   /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(Event $event, RulesChecker $rules)
    {
        return $rules;
    }

}
