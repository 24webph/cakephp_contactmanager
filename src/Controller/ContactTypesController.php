<?php
namespace ContactManager\Controller;

use ContactManager\Controller\AppController;

/**
 * ContactTypes Controller
 *
 * @property \ContactManager\Model\Table\ContactTypesTable $ContactTypes
 *
 * @method \ContactManager\Model\Entity\ContactType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $contactTypes = $this->paginate($this->ContactTypes);

        $this->set(compact('contactTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactType = $this->ContactTypes->get($id, [
            'contain' => ['Contacts']
        ]);

        $this->set('contactType', $contactType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactType = $this->ContactTypes->newEntity();
        if ($this->request->is('post')) {
            $contactType = $this->ContactTypes->patchEntity($contactType, $this->request->getData());
            if ($this->ContactTypes->save($contactType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$contactType->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('contactType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contactType = $this->ContactTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contactType = $this->ContactTypes->patchEntity($contactType, $this->request->getData());
            if ($this->ContactTypes->save($contactType)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$contact->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $this->set(compact('contactType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactType = $this->ContactTypes->get($id);
        if ($this->ContactTypes->delete($contactType)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
