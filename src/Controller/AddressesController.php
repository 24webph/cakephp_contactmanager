<?php
namespace ContactManager\Controller;

use ContactManager\Controller\AppController;

/**
 * Addresses Controller
 *
 * @property \ContactManager\Model\Table\AddressesTable $Addresses
 *
 * @method \ContactManager\Model\Entity\Address[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Locations', 'AddressTypes']
        ];
        $addresses = $this->paginate($this->Addresses);

        $this->set(compact('addresses'));
    }

    /**
     * View method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => ['Locations', 'AddressTypes']
        ]);

        $this->set('address', $address);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $address = $this->Addresses->newEntity();
        if ($this->request->is('post')) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$address->id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $locations = $this->Addresses->Locations->find('list', ['limit' => 200]);
        $addressTypes = $this->Addresses->AddressTypes->find('list', ['limit' => 200]);
        $this->set(compact('address', 'locations', 'addressTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('Saved with success.'));

                return $this->redirect(['action' => 'edit',$id]);
            }
            $this->Flash->error(__('Could not be saved. Please, try again.'));
        }
        $locations = $this->Addresses->Locations->find('list', ['limit' => 200]);
        $addressTypes = $this->Addresses->AddressTypes->find('list', ['limit' => 200]);
        $this->set(compact('address', 'locations', 'addressTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $address = $this->Addresses->get($id);
        if ($this->Addresses->delete($address)) {
            $this->Flash->success(__('Deleted with success.'));
        } else {
            $this->Flash->error(__('Could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
