# WharehouseManager plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require 24web/ContactManager
```

## DB Installation

Enable your plugin before

```
bin/cake migrations migrate -p 24web/ContactManager
```
